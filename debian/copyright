Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: github.com/prometheus/prometheus
Source: https://github.com/prometheus/prometheus
Files-Excluded:
 web/ui/build_ui.sh
 web/ui/module
 web/ui/package.json
 web/ui/package-lock.json
 web/ui/react-app
 web/ui/static/vendor/bootstrap-4.5.2
 web/ui/static/vendor/bootstrap3-typeahead
 web/ui/static/vendor/bootstrap4-glyphicons/css/bootstrap-glyphicons.min.css
 web/ui/static/vendor/bootstrap4-glyphicons/fonts
 web/ui/static/vendor/bootstrap4-glyphicons/maps
 web/ui/static/vendor/eonasdan-bootstrap-datetimepicker
 web/ui/static/vendor/js/jquery-3.5.1.min.js
 web/ui/static/vendor/js/jquery.hotkeys.js
 web/ui/static/vendor/js/popper.min.js
 web/ui/static/vendor/moment
 web/ui/static/vendor/mustache
 web/ui/static/vendor/rickshaw

Files: *
Copyright: 2012-2022 The Prometheus Authors
License: Apache-2.0

Files: debian/*
Copyright: 2022 Daniel Swarbrick <dswarbrick@debian.org>
License: Apache-2.0

Files: web/ui/static/vendor/js/jquery.selection.js
Copyright: 2010-2012 IWASAKI Koji (@madapaja).
License: Expat
Comment: Although not documented upstream, this file corresponds with commit
 90736ca5c97e603af0c0c1c823cb340860926077 in
 https://github.com/madapaja/jquery.selection/

Files: debian/missing-sources/bootstrap3-typeahead/*
Copyright: 2014 Bass Jobsen @bassjobsen
License: Apache-2.0
Comment: The minified files were replaced by the original sources from
 https://github.com/bassjobsen/Bootstrap-3-Typeahead at tag v3.1.1 (to exactly
 match what was originally shipped).

Files: web/ui/static/vendor/bootstrap4-glyphicons/*
Copyright: 2011-2018 Twitter, Inc.
License: Expat
Comment: Instead of leaving all the vendored files from this project, only the
 CSS file was retained, which serves as glue with the Debian-packaged glyphicons
 halflings fonts.

Files: web/ui/static/vendor/fuzzy/*
Copyright: 2015 Matt York
License: Expat
Comment: Embedded copy with some local changes by Or Cohen <orc@fewbytes.com>

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
Comment:
 On Debian systems, the full text of the Apache License version 2 can be found
 in the file `/usr/share/common-licenses/Apache-2.0'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
